## GAM Bulk Calendar Wrangler
This assumes you already have [GAM](https://github.com/GAM-team/GAM) installed and configured.

Google Workspace's built in tools for managing existing calendars across a domain are... nonexistant.

# This script uses GAM and Powershell to do the following:
1. Compile a list of every Calendar associated with every user in the domain
2. Dump that list into a .csv file
3. Read that .csv file into an array
4. Filter that array to remove:
    - Personal calendars
    - Google Classroom calendars
    - Calendars not owned by users in your domain
    - Calendars already owned by your specified user
5. Add the specified user as an owner to each calendar
6. Make the calendar visible to the user on Google Calendars

# Before running this you must specify:
1. A valid account name to attach calendars ($CALMASTER). This should likely be a service account.
2. A complete path to the .csv file containing the list. ($CSVPATH)

# The process of fetching the Calendars from Google Workspace via GAM can take a very long time.
This script prompts you to skip that step if you already have populated the list.

Feel free to use this script for whatever purposes you like.