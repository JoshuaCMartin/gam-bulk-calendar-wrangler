<# Set CALMASTER to the user you'd like to become owner of all calendars. #>
$CALMASTER = "Calendar.Owner@example.org"
<# Output path for list of calendars. #>
$CSVPATH = "C:\calendars.csv"

<# Prompt to update list, takes a VERY long time. #>
$choice0 = Read-Host "Update list of Calendars? y/n"
if ($choice0 -ieq "y") {
    <# Pull list of calendars and dump into CSV. TAKES A VERY LONG TIME #>
    <# This may throw an NativeCommandError. It can safely be ignored.#>
    gam all users print calendars > $CSVPATH
} 
<# Check for existence of Calendar list file. #>
if (Test-Path $CSVPATH) {
    <# Import Calendar List File #>
    $calendars = import-csv -path $CSVPATH
    <# Filter Calendar list #>
    <# # Include shared calendars, ignore personal. #>
    <# Ignore Google Classroom Calendars #>
    <# Include calendars owned by users, do not include external. #>
    <# Ignore calendars already owned by CALMASTER #>
    $calendars = $calendars | Where-Object {`
    ($_.id -like "*group.calendar.google.com")`
    -and ($_.id -notlike "*classroom*")`
    -and ($_.accessRole -like "owner")`
    -and ($_.primaryEmail -notlike $CALMASTER) } 

    <# Output calendar count, prompt to continue. #>
    $choice1 = Read-Host $calendars.length " valid calendars found. Take ownership? y/n"
    if ($choice1 -ieq "y") {
        foreach ($calendar in $calendars) {
            write-host "Taking ownership of " $calendar.id
            <# Add CALMASTER as Owner #>
            <# This step may throw 403 errors. They can safely be ignored #>
            gam calendar $calendar.id add owner $CALMASTER sendnotifications false
            write-host "Adding " $calendar.id " to PSD.Calendar"
            <# Make calendar visible to CALMASTER. #>
            gam user $CALMASTER add calendar $CALMASTER.id selected false
        }
    }
} else {
    write-host $CSVPATH " not found."
}
